package com.example.demo.controller;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.Set;

import com.example.demo.model.Employee;
import com.example.demo.model.EmployeeInProject;
import com.example.demo.model.Project;
import com.example.demo.model.Role;
import com.example.demo.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @PostMapping("/project")
    public ResponseEntity<Project> createProject(@RequestBody Project project) {

        Project savedProject = projectService.saveProject(project);

        if (savedProject != null)
            return new ResponseEntity<>(savedProject, CREATED);

        return new ResponseEntity<>(BAD_REQUEST);
    }

    @PostMapping("/projectAddEmployee")
    public ResponseEntity<Project> addEmployeeToProject(@RequestParam Long employeeID, @RequestParam Long projectID, @RequestBody Role role,
            @RequestParam int hours) {
        Project changedProject = projectService.addEmployeeToProject(employeeID, projectID, role, hours);

        if (changedProject != null)
            return new ResponseEntity<>(changedProject, OK);

        return new ResponseEntity<>(BAD_REQUEST);
    }

    @GetMapping("/proposeEmployeesForProject")
    public ResponseEntity<List<Employee>> proposeEmployeesForProject(@RequestParam long projectID) {
        List<Employee> employees = projectService.proposeEmployeesForProject(projectID);

        if (employees != null)
            return new ResponseEntity<>(employees, OK);

        return new ResponseEntity<>(BAD_REQUEST);
    }

    @GetMapping("EmployeesInProject")
    public ResponseEntity<Set<EmployeeInProject>> getEmployeesInProject(@RequestParam long projectID) {
        Set<EmployeeInProject> employeeInProjects = projectService.getEmployeesInProject(projectID);

        if (employeeInProjects != null)
            return new ResponseEntity<>(employeeInProjects, OK);

        return new ResponseEntity<>(BAD_REQUEST);
    }
}
