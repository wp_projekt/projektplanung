package com.example.demo.model;

import java.io.Serializable;

public class ProjectRequiredRolesPK implements Serializable {

    private long role;
    private long project;

    public long getRole() {
        return role;
    }

    public void setRole(long role) {
        this.role = role;
    }

    public long getProject() {
        return project;
    }

    public void setProject(long project) {
        this.project = project;
    }
}
