package com.example.demo.service;

import java.util.Optional;
import java.util.Set;

import com.example.demo.model.Address;
import com.example.demo.model.Employee;
import com.example.demo.model.EmployeeInProject;
import com.example.demo.repository.AddressRepository;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private AddressRepository addressRepository;

    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Employee changeAddress(long addressID, long employeeID) {

        Optional<Employee> employeeOptional = employeeRepository.findById(employeeID);
        if (employeeOptional.isEmpty()) {
            return null;
        }

        Optional<Address> addressOptional = addressRepository.findById(addressID);
        if (addressOptional.isEmpty()) {
            return null;
        }

        Employee employee = employeeOptional.get();
        employee.setAddress(addressOptional.get());

        return employeeRepository.save(employee);
    }

    public Set<EmployeeInProject> getProjectsOfEmployee(long employeeID) {

        Optional<Employee> employeeOptional = employeeRepository.findById(employeeID);
        if (employeeOptional.isEmpty()) {
            return null;
        }

        Employee employee = employeeOptional.get();

        return employee.getProjects();
    }

    public Employee setWorkedHours(long employeeID, Long projectID, int amountOfHours) {

        Optional<Employee> employeeOptional = employeeRepository.findById(employeeID);
        if (employeeOptional.isEmpty()) {
            return null;
        }

        Employee employee = employeeOptional.get();

        if(employee.getProjects()==null)
            return null;

        for (EmployeeInProject employeeInProject : employee.getProjects()) {
            if (employeeInProject.getProject().getId() == projectID) {
                employeeInProject.setWorkedHours(amountOfHours);
                return saveEmployee(employee);
            }
        }
        return null;
    }
}
