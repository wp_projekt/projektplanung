package com.example.demo.controller;

import com.example.demo.model.*;
import com.example.demo.service.ProjectService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ProjectController.class)
public class ProjectControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ProjectService projectService;

    private Project projectSample;
    private Employee employee;
    private Role role;
    private EmployeeInProject employeeInProject;

    @BeforeEach
    void setUp() {
        projectSample = new Project();
        projectSample.setName("projectName");
        employee = new Employee();
        employee.setFirstName("FirstNameEmployee");
        role = new Role();
        role.setName("RoleName");
    }

    @Test
    void testPostSaveProjectSuccessfully() throws Exception {
        String projectJSON = objectMapper.writeValueAsString(projectSample);
        when(projectService.saveProject(any(Project.class))).thenReturn(projectSample);

        ResultActions result = mockMvc.perform(post("/project")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("projectName"));

    }

    @Test
    void testPostSaveProjectNotSuccessfully() throws Exception {
        String projectJSON = objectMapper.writeValueAsString(projectSample);

        ResultActions result = mockMvc.perform(post("/project")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isBadRequest());
    }

    @Test
    void testPostProjectAddEmployeeSuccessfully() throws Exception {
        String projectJSON = objectMapper.writeValueAsString(role);
        when(projectService.addEmployeeToProject(anyLong(), anyLong(), any(Role.class), anyInt())).thenReturn(projectSample);

        ResultActions result = mockMvc.perform(post("/projectAddEmployee?employeeID=1&projectID=2&hours=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isOk())
                .andExpect(mvcResult -> assertThat(mvcResult.getResponse().getContentAsString(), CoreMatchers.containsString("projectName")));
    }

    @Test
    void testPostProjectAddEmployeeNotSuccessfullyMissingParams() throws Exception {
        String projectJSON = objectMapper.writeValueAsString(role);
        when(projectService.addEmployeeToProject(anyLong(), anyLong(), any(Role.class), anyInt())).thenReturn(null);

        ResultActions result = mockMvc.perform(post("/projectAddEmployee")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isBadRequest())
                .andExpect(mockMvc -> assertThat(mockMvc.getResolvedException().getMessage(), is("Required Long parameter 'employeeID' is not present")));
    }

    @Test
    void testPostProjectAddEmployeeNotSuccessfully() throws Exception {
        String projectJSON = objectMapper.writeValueAsString(projectSample);
        when(projectService.addEmployeeToProject(anyLong(), anyLong(), any(Role.class), anyInt())).thenReturn(null);

        ResultActions result = mockMvc.perform(post("/projectAddEmployee?employeeID=1&projectID=2&hours=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isBadRequest());
    }

    @Test
    void testGetProposeEmployeesForProjectSuccessfully() throws Exception {
        List<Employee> employeeList = new LinkedList<>();
        employeeList.add(employee);
        String projectJSON = objectMapper.writeValueAsString(projectSample);
        when(projectService.proposeEmployeesForProject(anyLong())).thenReturn(employeeList);

        ResultActions result = mockMvc.perform(get("/proposeEmployeesForProject?projectID=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isOk())
                .andExpect(mvcResult -> assertThat(mvcResult.getResponse().getContentAsString(), CoreMatchers.containsString("FirstNameEmployee")));
    }

    @Test
    void testGetProposeEmployeesForProjectNotSuccessfullyMissingParams() throws Exception {
        String projectJSON = objectMapper.writeValueAsString(projectSample);

        ResultActions result = mockMvc.perform(get("/proposeEmployeesForProject?")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isBadRequest())
                .andExpect(mockMvc -> assertThat(mockMvc.getResolvedException().getMessage(), is("Required long parameter 'projectID' is not present")));
    }

    @Test
    void testGetProposeEmployeesForProjectNotSuccessfully() throws Exception {
        List<Employee> employeeList = new LinkedList<>();
        employeeList.add(employee);
        String projectJSON = objectMapper.writeValueAsString(projectSample);
        when(projectService.proposeEmployeesForProject(anyLong())).thenReturn(null);

        ResultActions result = mockMvc.perform(get("/proposeEmployeesForProject?projectID=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isBadRequest());
    }

    @Test
    void testGetEmployeesInProjectSuccessfully() throws Exception {
        Set<EmployeeInProject> employeeSet = new HashSet<>();
        employeeSet.add(new EmployeeInProject(employee, projectSample, 2, new Date(1234), new Date(12)));
        String projectJSON = objectMapper.writeValueAsString(employeeSet);
        when(projectService.getEmployeesInProject(anyLong())).thenReturn(employeeSet);

        ResultActions result = mockMvc.perform(get("/EmployeesInProject?projectID=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isOk())
                .andExpect(mvcResult -> assertThat(mvcResult.getResponse().getContentAsString(), CoreMatchers.containsString("FirstNameEmployee")));
    }

    @Test
    void testGetEmployeesInProjectNotSuccessfullyMissingParams() throws Exception {
        String projectJSON = objectMapper.writeValueAsString(projectSample);

        ResultActions result = mockMvc.perform(get("/EmployeesInProject")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isBadRequest())
                .andExpect(mockMvc -> assertThat(mockMvc.getResolvedException().getMessage(), is("Required long parameter 'projectID' is not present")));
    }

    @Test
    void testGetEmployeesInProjectNotSuccessfully() throws Exception {
        Set<EmployeeInProject> employeeSet = new HashSet<>();
        employeeSet.add(new EmployeeInProject(employee, projectSample, 2, new Date(1234), new Date(12)));
        String projectJSON = objectMapper.writeValueAsString(employeeSet);
        when(projectService.getEmployeesInProject(anyLong())).thenReturn(null);

        ResultActions result = mockMvc.perform(get("/EmployeesInProject?projectID=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJSON));

        result.andExpect(status().isBadRequest());
    }

}
