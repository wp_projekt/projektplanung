package com.example.demo.controller;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.util.Set;

import com.example.demo.model.Employee;
import com.example.demo.model.EmployeeInProject;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/employee")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {

        Employee savedEmployee = employeeService.saveEmployee(employee);

        if (savedEmployee != null)
            return new ResponseEntity<>(savedEmployee, CREATED);

        return new ResponseEntity<>(BAD_REQUEST);
    }

    @PostMapping("/employeeChangeAddress")
    public ResponseEntity<Employee> changeAddress(@RequestParam long addressID, @RequestParam long employeeID) {

        Employee changedEmployee = employeeService.changeAddress(addressID, employeeID);

        if (changedEmployee != null)
            return new ResponseEntity<>(changedEmployee, OK);

        return new ResponseEntity<>(BAD_REQUEST);
    }

    @GetMapping("/projectsOfEmployee")
    public ResponseEntity<Set<EmployeeInProject>> getProjectsOfEmployee(@RequestParam long employeeID) {
        Set<EmployeeInProject> employeeInProjects = employeeService.getProjectsOfEmployee(employeeID);

        if (employeeInProjects != null)
            return new ResponseEntity<>(employeeInProjects, OK);

        return new ResponseEntity<>(BAD_REQUEST);
    }

    @PostMapping("/employeeAddWorkedHours")
    public ResponseEntity<Employee> addWorkedHoursToProject(@RequestParam long employeeID, @RequestParam Long projectID,
            @RequestParam int amountOfHours) {
        Employee employee = employeeService.setWorkedHours(employeeID, projectID, amountOfHours);

        if (employee != null)
            return new ResponseEntity<>(employee, OK);

        return new ResponseEntity<>(BAD_REQUEST);

    }
}
