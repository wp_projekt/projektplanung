package com.example.demo.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import com.example.demo.model.Address;
import com.example.demo.model.Employee;
import com.example.demo.model.EmployeeInProject;
import com.example.demo.model.Project;
import com.example.demo.model.ProjectRequiredRoles;
import com.example.demo.repository.AddressRepository;
import com.example.demo.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class EmployeeServiceTest {

    @MockBean
    private AddressRepository addressRepository;

    @MockBean
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeService employeeService;

    private Employee employeeSample;
    private Address addressSample;
    private Address addressNew;
    private Project projectSample;

    @BeforeEach
    void setUp() throws ParseException {
        employeeSample = new Employee();
        addressSample = new Address();
        addressNew = new Address();
        projectSample = new Project();
        ProjectRequiredRoles projectRequiredRolesSample = new ProjectRequiredRoles();

        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-10");

        addressSample.setId(1L);
        addressSample.setStreet("Street");
        addressSample.setPostCode("1234");
        addressSample.setMail("mail@mail.com");
        addressSample.setCity("city");

        addressNew.setId(2L);
        addressNew.setStreet("Street");
        addressNew.setPostCode("1234");
        addressNew.setMail("mail@mail.com");
        addressNew.setCity("city");

        projectRequiredRolesSample.setProject(projectSample);
        projectRequiredRolesSample.setHours(8);

        projectSample.setId(1L);
        projectSample.setStartDate(startDate);
        projectSample.setEndDate(endDate);
        projectSample.setRequiredRoles(new HashSet<>(Collections.singletonList(projectRequiredRolesSample)));
        projectSample.setRequiredNumberOfEmployees(10);

        EmployeeInProject employeeInProjectSample = new EmployeeInProject(employeeSample, projectSample, 8, startDate, endDate);
        Set<EmployeeInProject> projectSet = new HashSet<>();
        projectSet.add(employeeInProjectSample);
        projectSample.setEmployees(new HashSet<>(Collections.singletonList(employeeInProjectSample)));

        employeeSample.setId(1L);
        employeeSample.setAddress(addressSample);
        employeeSample.setProjects(projectSet);
    }

    @Test
    void testSaveEmployee() {
        when(employeeRepository.save(employeeSample)).thenReturn(employeeSample);

        Employee employeeResult = employeeService.saveEmployee(employeeSample);

        Assertions.assertEquals(employeeResult, employeeSample);
    }

    @Test
    void testChangeAddressEmployeeIsNull() {
        employeeSample.setId(0);

        Employee employeeResult = employeeService.changeAddress(addressSample.getId(), employeeSample.getId());

        Assertions.assertNull(employeeResult);
    }

    @Test
    void testChangeAddressAddressIsNull() {
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));
        when(addressRepository.findById(1L)).thenReturn(Optional.empty());

        Employee employeeResult = employeeService.changeAddress(1L, 1L);

        Assertions.assertNull(employeeResult);
    }

    @Test
    void testChangeAddressSuccessfully() {
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));
        when(addressRepository.findById(2L)).thenReturn(Optional.ofNullable(addressNew));
        when(employeeRepository.save(any(Employee.class))).thenReturn(employeeSample);

        Assertions.assertNotEquals(addressNew.getId(), employeeSample.getAddress().getId());

        Employee employeeResult = employeeService.changeAddress(2L, 1L);

        Assertions.assertNotNull(employeeResult.getAddress());
        Assertions.assertEquals(employeeResult.getAddress().getId(), addressNew.getId());
    }

    @Test
    void testGetProjectsOfEmployeeNotSuccessfullyEmployeeIsNull() {
        when(employeeRepository.findById(1L)).thenReturn(Optional.empty());

        Set<EmployeeInProject> employeeResult = employeeService.getProjectsOfEmployee(1L);

        Assertions.assertNull(employeeResult);
    }

    @Test
    void testGetProjectsOfEmployeeSuccessfully() {
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Set<EmployeeInProject> employeeResult = employeeService.getProjectsOfEmployee(1L);

        Assertions.assertEquals(1, employeeResult.size());
    }

    @Test
    void testSetWorkedHoursNotSuccessfullyEmployeeIsNull() {
        when(employeeRepository.findById(1L)).thenReturn(Optional.empty());

        Employee employeeResult = employeeService.setWorkedHours(1L, 2L, 2);

        Assertions.assertNull(employeeResult);

    }

    @Test
    void testSetWorkedHoursNotSuccessfullyEmployeeIsNotInProjects() {
        employeeSample.setProjects(null);
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Employee employeeResult = employeeService.setWorkedHours(1L, 2L, 2);

        Assertions.assertNull(employeeResult);
    }

    @Test
    void testSetWorkedHoursNotSuccessfullyEmployeeIsOnlyInOtherProjects() throws ParseException {
        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2010-01-01");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2010-01-10");
        EmployeeInProject newProject = new EmployeeInProject(employeeSample, projectSample, 9, startDate, endDate);
        Set<EmployeeInProject> newProjectSet = new HashSet<>();
        newProjectSet.add(newProject);
        employeeSample.setProjects(newProjectSet);
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Employee employeeResult = employeeService.setWorkedHours(1L, 2L, 2);

        Assertions.assertNull(employeeResult);
    }

    @Test
    void testSetWorkedHoursSuccessfully() {
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));
        when(employeeRepository.save(any(Employee.class))).thenReturn(employeeSample);

        Employee employeeResult = employeeService.setWorkedHours(1L, 1L, 2);

        Assertions.assertNotNull(employeeResult);
    }
}
