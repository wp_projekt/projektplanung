package com.example.demo.service;

import java.time.Duration;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.example.demo.model.Employee;
import com.example.demo.model.EmployeeInProject;
import com.example.demo.model.Project;
import com.example.demo.model.ProjectRequiredRoles;
import com.example.demo.model.Role;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

    public static final int EIGHT_HOURS = 8;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public Project saveProject(Project project) {

        if (project.getRequiredRoles() == null || project.getRequiredNumberOfEmployees() == 0)
            return null;

        if (project.getStartDate().before(project.getEndDate()))
            return projectRepository.save(project);

        return null;
    }

    public Project addEmployeeToProject(Long employeeID, Long projectID, Role role, int plannedHours) {

        Optional<Project> projectOptional = projectRepository.findById(projectID);
        if (projectOptional.isEmpty())
            return null;

        Optional<Employee> employeeOptional = employeeRepository.findById(employeeID);
        if (employeeOptional.isEmpty())
            return null;

        Project project = projectOptional.get();
        Employee employee = employeeOptional.get();

        if (!checkAmountOfEmployees(project) || !checkRoleOfEmployee(employee, role) || isEmployeeNotAvailable(project, employee) || maxOfHoursExceeded(role,
                project, plannedHours))
            return null;

        Date endDateForEmployeeAtThisProject = calculateNewEndDateForEmployeeByGivenHours(project, plannedHours);
        EmployeeInProject employeeInProject = new EmployeeInProject(employee, project, plannedHours, project.getStartDate(), endDateForEmployeeAtThisProject);

        if (employee.getProjects() == null) {
            employee.setProjects(new HashSet<>(Collections.singletonList(employeeInProject)));
        } else {
            avoidRedundantData(project, employee);
            employee.getProjects().add(employeeInProject);
        }

        if (project.getEmployees() == null) {
            project.setEmployees(new HashSet<>(Collections.singletonList(employeeInProject)));
        } else {
            avoidRedundantData(project, employee);
            project.getEmployees().add(employeeInProject);
        }

        employeeRepository.save(employee);
        projectRepository.save(project);

        return project;
    }

    public List<Employee> proposeEmployeesForProject(long projectID) {

        Optional<Project> projectOptional = projectRepository.findById(projectID);
        if (projectOptional.isEmpty())
            return null;

        Project project = projectOptional.get();

        List<Employee> employees = employeeRepository.findAll();

        if (employees.isEmpty())
            return null;

        employees.removeIf(employee -> !checkEmployeeContainsNeededRole(employee.getRoles(), project.getRequiredRoles()) || isEmployeeNotAvailable(project,
                employee));

        return employees;
    }

    public Set<EmployeeInProject> getEmployeesInProject(long projectID) {
        Optional<Project> projectOptional = projectRepository.findById(projectID);
        if (projectOptional.isEmpty())
            return null;

        Project project = projectOptional.get();

        return project.getEmployees();
    }

    private boolean checkEmployeeContainsNeededRole(Set<Role> roles, Set<ProjectRequiredRoles> requiredRoles) {
        for (ProjectRequiredRoles projectRequiredRoles : requiredRoles) {
            if (roles.contains(projectRequiredRoles.getRole())) {
                return true;
            }
        }
        return false;
    }

    private Date calculateNewEndDateForEmployeeByGivenHours(Project project, int plannedHours) {
        int daysToReduce = plannedHours / 8;

        if (plannedHours % 8 != 0)
            daysToReduce++;

        return new Date(project.getEndDate().getTime() - Duration.ofDays(daysToReduce).toMillis());

    }

    private boolean isEmployeeNotAvailable(Project project, Employee employee) {

        if (employee.getProjects() == null)
            return false;

        for (EmployeeInProject employeeInProject : employee.getProjects()) {
            if (employeeInProject.getStartDate().getTime() <= project.getEndDate().getTime() && project.getStartDate().getTime() <= employeeInProject
                    .getEndDate().getTime())
                return true;
        }
        return false;
    }

    private boolean checkRoleOfEmployee(Employee employee, Role role) {
        return employee.getRoles().contains(role);
    }

    private boolean checkAmountOfEmployees(Project project) {

        if (project.getEmployees() == null)
            return true;

        return project.getEmployees().size() < project.getRequiredNumberOfEmployees();
    }

    private boolean maxOfHoursExceeded(Role role, Project project, int plannedHours) {
        for (ProjectRequiredRoles projectRequiredRoles : project.getRequiredRoles()) {
            if (projectRequiredRoles.getRole().equals(role) && projectRequiredRoles.getHours() < plannedHours) {
                projectRequiredRoles.setHours(projectRequiredRoles.getHours() - plannedHours);
                return true;
            }
        }
        return false;
    }

    private void avoidRedundantData(Project project, Employee employee) {
        project.getEmployees().removeIf(employeeInProject -> employeeInProject.getEmployee().getId() == employee.getId());
        employee.getProjects().removeIf(employeeInProject -> employeeInProject.getProject().getId() == project.getId());
    }
}
