package com.example.demo.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String name;

    @NotNull
    private Date startDate;

    @NotNull
    private Date endDate;

    @NotNull
    private int requiredNumberOfEmployees;

    @OneToMany(mappedBy = "project")
    private Set<ProjectRequiredRoles> requiredRoles;

    @OneToMany(mappedBy = "project")
    private Set<EmployeeInProject> employees;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getRequiredNumberOfEmployees() {
        return requiredNumberOfEmployees;
    }

    public void setRequiredNumberOfEmployees(int requiredNumberOfEmployees) {
        this.requiredNumberOfEmployees = requiredNumberOfEmployees;
    }

    public Set<ProjectRequiredRoles> getRequiredRoles() {
        return requiredRoles;
    }

    public void setRequiredRoles(Set<ProjectRequiredRoles> requiredRoles) {
        this.requiredRoles = requiredRoles;
    }

    public Set<EmployeeInProject> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<EmployeeInProject> employees) {
        this.employees = employees;
    }
}
