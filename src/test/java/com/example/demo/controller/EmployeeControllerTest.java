package com.example.demo.controller;

import com.example.demo.model.Employee;
import com.example.demo.model.EmployeeInProject;
import com.example.demo.model.Project;
import com.example.demo.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = EmployeeController.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EmployeeService employeeService;

    private Employee employee;

    @BeforeEach
    void setUp() {
        employee = new Employee();
        employee.setFirstName("firstName");
        employee.setLastName("lastName");
    }

    @Test
    void testSaveEmployeeSuccessfully() throws Exception {
        String employeeJSON = objectMapper.writeValueAsString(employee);
        when(employeeService.saveEmployee(any(Employee.class))).thenReturn(employee);

        ResultActions result = mockMvc.perform(post("/employee")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeJSON));

        result.andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName").value("firstName"))
                .andExpect(jsonPath("$.lastName").value("lastName"));
    }

    @Test
    void testSaveEmployeeNotSuccessfully() throws Exception {
        String employeeJSON = objectMapper.writeValueAsString(employee);
        when(employeeService.saveEmployee(any(Employee.class))).thenReturn(null);

        ResultActions result = mockMvc.perform(post("/employee")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeJSON));

        result.andExpect(status().isBadRequest());
    }

    @Test
    void testChangeEmployeeAddressSuccessfully() throws Exception {
        String employeeJSON = objectMapper.writeValueAsString(employee);
        when(employeeService.changeAddress(any(Long.class), any(Long.class))).thenReturn(employee);

        ResultActions result = mockMvc.perform(post("/employeeChangeAddress?addressID=1&employeeID=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeJSON));

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("firstName"))
                .andExpect(jsonPath("$.lastName").value("lastName"));
    }

    @Test
    void testChangeEmployeeAddressNotSuccessfully() throws Exception {
        String employeeJSON = objectMapper.writeValueAsString(employee);
        when(employeeService.changeAddress(any(Long.class), any(Long.class))).thenReturn(null);

        ResultActions result = mockMvc.perform(post("/employeeChangeAddress?addressID=1&employeeID=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeJSON));

        result.andExpect(status().isBadRequest());
    }

    @Test
    void testGetProjectsOfEmployeeSuccessfully() throws Exception {
        Project project = new Project();
        project.setName("projectName");

        Set<EmployeeInProject> employeeInProjects = new HashSet<>();
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        EmployeeInProject employeeInProject = new EmployeeInProject(employee, project, 100, format.parse("01.01.2021"), format.parse("01.03.2021"));
        employeeInProjects.add(employeeInProject);

        String employeeInProjectsJSON = objectMapper.writeValueAsString(employeeInProjects);
        when(employeeService.getProjectsOfEmployee(any(Long.class))).thenReturn(employeeInProjects);

        ResultActions result = mockMvc.perform(get("/projectsOfEmployee?employeeID=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeInProjectsJSON));

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$[0].employee.firstName").value("firstName"))
                .andExpect(jsonPath("$[0].employee.lastName").value("lastName"))
                .andExpect(jsonPath("$[0].project.name").value("projectName"));
    }

    @Test
    void testGetProjectsOfEmployeeNotSuccessfully() throws Exception {
        Project project = new Project();
        project.setName("projectName");

        Set<EmployeeInProject> employeeInProjects = new HashSet<>();
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        EmployeeInProject employeeInProject = new EmployeeInProject(employee, project, 100, format.parse("01.01.2021"), format.parse("01.03.2021"));
        employeeInProjects.add(employeeInProject);

        String employeeInProjectsJSON = objectMapper.writeValueAsString(employeeInProjects);
        when(employeeService.getProjectsOfEmployee(any(Long.class))).thenReturn(null);

        ResultActions result = mockMvc.perform(get("/projectsOfEmployee?employeeID=1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeInProjectsJSON));

        result.andExpect(status().isBadRequest());
    }

    @Test
    void testAddWorkedHoursToProjectSuccessfully() throws Exception {
        String employeeJSON = objectMapper.writeValueAsString(employee);
        when(employeeService.setWorkedHours(any(Long.class), any(Long.class), any(Integer.class))).thenReturn(employee);

        ResultActions result = mockMvc.perform(post("/employeeAddWorkedHours?employeeID=1&projectID=1&amountOfHours=8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeJSON));

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("firstName"))
                .andExpect(jsonPath("$.lastName").value("lastName"));
    }

    @Test
    void testAddWorkedHoursToProjectNotSuccessfully() throws Exception {
        String employeeJSON = objectMapper.writeValueAsString(employee);
        when(employeeService.setWorkedHours(any(Long.class), any(Long.class), any(Integer.class))).thenReturn(null);

        ResultActions result = mockMvc.perform(post("/employeeAddWorkedHours?employeeID=1&projectID=1&amountOfHours=8")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeJSON));

        result.andExpect(status().isBadRequest());
    }
}
