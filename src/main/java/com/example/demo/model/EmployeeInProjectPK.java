package com.example.demo.model;

import java.io.Serializable;

public class EmployeeInProjectPK implements Serializable {

    private long employee;
    private long project;

    public long getEmployee() {
        return employee;
    }

    public void setEmployee(long employee) {
        this.employee = employee;
    }

    public long getProject() {
        return project;
    }

    public void setProject(long project) {
        this.project = project;
    }
}
