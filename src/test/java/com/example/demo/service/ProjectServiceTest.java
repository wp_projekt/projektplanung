package com.example.demo.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.example.demo.model.Employee;
import com.example.demo.model.EmployeeInProject;
import com.example.demo.model.Project;
import com.example.demo.model.ProjectRequiredRoles;
import com.example.demo.model.Role;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.repository.ProjectRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class ProjectServiceTest {

    @MockBean
    private ProjectRepository projectRepository;

    @MockBean
    private EmployeeRepository employeeRepository;

    @Autowired
    private ProjectService projectService;

    private Employee employeeSample;
    private Project projectSample;
    private Role roleSample;
    private EmployeeInProject employeeInProjectSample;

    @BeforeEach
    void setUp() throws ParseException {
        employeeSample = new Employee();
        projectSample = new Project();
        ProjectRequiredRoles projectRequiredRolesSample = new ProjectRequiredRoles();
        roleSample = new Role();

        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-10");

        roleSample.setName("Developer");

        employeeSample.setId(1L);
        employeeSample.setRoles(new HashSet<>(Collections.singletonList(roleSample)));

        projectRequiredRolesSample.setProject(projectSample);
        projectRequiredRolesSample.setHours(8);
        projectRequiredRolesSample.setRole(roleSample);

        projectSample.setId(1L);
        projectSample.setStartDate(startDate);
        projectSample.setEndDate(endDate);
        projectSample.setRequiredRoles(new HashSet<>(Collections.singletonList(projectRequiredRolesSample)));
        projectSample.setRequiredNumberOfEmployees(10);

        employeeInProjectSample = new EmployeeInProject(employeeSample, projectSample, 8, startDate, endDate);
        projectSample.setEmployees(new HashSet<>(Collections.singletonList(employeeInProjectSample)));
    }

    @Test
    void testSaveProjectSuccessfullyStartDateIsBeforeEndDateAndMandatoryFieldsNotNull() {
        when(projectRepository.save(projectSample)).thenReturn(projectSample);

        Project projectResult = projectService.saveProject(projectSample);

        Assertions.assertEquals(projectResult, projectSample);
    }

    @Test
    void testSaveProjectNotSuccessfullyRequiredRolesAreNull() {
        projectSample.setRequiredRoles(null);

        Project projectResult = projectService.saveProject(projectSample);

        Assertions.assertNull(projectResult);
    }

    @Test
    void testSaveProjectNotSuccessfullyRequiredNumberOfEmployeesIsZero() {
        projectSample.setRequiredNumberOfEmployees(0);

        Project projectResult = projectService.saveProject(projectSample);

        Assertions.assertNull(projectResult);
    }

    @Test
    void testSaveProjectNotSuccessfullyStartDateIsAfterEndDate() throws ParseException {
        projectSample.setStartDate(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-02"));
        projectSample.setEndDate(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01"));

        Project projectResult = projectService.saveProject(projectSample);

        Assertions.assertNull(projectResult);
    }

    @Test
    void testAddEmployeeToProjectNotSuccessfullyProjectIsNull() {
        when(projectRepository.findById(1L)).thenReturn(Optional.empty());

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 8);

        Assertions.assertNull(projectResult);
    }

    @Test
    void testAddEmployeeToProjectNotSuccessfullyEmployeeIsNull() {
        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.empty());

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 8);
        Assertions.assertNull(projectResult);
    }

    @Test
    void testAddEmployeeToProjectNotSuccessfullyMaxAmountOfEmployeesReached() {
        projectSample.setRequiredNumberOfEmployees(1);

        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 8);

        Assertions.assertNull(projectResult);
    }

    @Test
    void testAddEmployeeToProjectNotSuccessfullyEmployeeHasNotNeededRole() {
        Role otherRoleSample = new Role();
        otherRoleSample.setName("Scrum Master");

        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, otherRoleSample, 8);

        Assertions.assertNull(projectResult);
    }

    @Test
    void testAddEmployeeToProjectNotSuccessfullyEmployeeIsNotAvailableBecauseEndDateOfOtherProjectOverlapsWithStartDateOfCurrentProject()
            throws ParseException {
        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse("1999-12-31");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01");
        EmployeeInProject otherEmployeeInProject = new EmployeeInProject(employeeSample, projectSample, 8, startDate, endDate);
        employeeSample.setProjects(new HashSet<>(Collections.singletonList(otherEmployeeInProject)));

        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 8);

        Assertions.assertNull(projectResult);
    }

    @Test
    void testAddEmployeeToProjectNotSuccessfullyEmployeeIsNotAvailableBecauseStartDateOfOtherProjectOverlapsWithEndDateOfCurrentProject()
            throws ParseException {
        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-10");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-11");
        EmployeeInProject otherEmployeeInProject = new EmployeeInProject(employeeSample, projectSample, 8, startDate, endDate);
        employeeSample.setProjects(new HashSet<>(Collections.singletonList(otherEmployeeInProject)));

        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 8);

        Assertions.assertNull(projectResult);
    }

    @Test
    void testAddEmployeeToProjectNotSuccessfullyMaxOfPlannedHoursForRoleExceeded() {

        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 9);

        Assertions.assertNull(projectResult);
    }

    @Test
    void testAddEmployeeToProjectSuccessfullyEmployeeIsNotInOtherProjects() throws ParseException {
        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 8);
        EmployeeInProject employeeInProjectResult = projectResult.getEmployees().iterator().next();
        employeeInProjectSample.setEndDate(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-09"));

        assertThat(employeeInProjectResult, samePropertyValuesAs(employeeInProjectSample));
    }

    @Test
    void testAddEmployeeToProjectSuccessfullyConsiderSubtractionOfAnNotFullUsedDay() throws ParseException {
        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));
        projectSample.getRequiredRoles().iterator().next().setHours(9);

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 9);
        EmployeeInProject employeeInProjectResult = projectResult.getEmployees().iterator().next();
        employeeInProjectSample.setEndDate(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-08"));
        employeeInProjectSample.setPlannedHours(9);

        assertThat(employeeInProjectResult, samePropertyValuesAs(employeeInProjectSample));
    }

    @Test
    void testAddEmployeeToProjectSuccessfullyEmployeeIsInOtherProjects() throws ParseException {
        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-02-10");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-02-11");
        EmployeeInProject otherEmployeeInProject = new EmployeeInProject(employeeSample, projectSample, 8, startDate, endDate);
        employeeSample.setProjects(new HashSet<>(Collections.singletonList(otherEmployeeInProject)));
        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 8);
        EmployeeInProject employeeInProjectResult = projectResult.getEmployees().iterator().next();
        employeeInProjectSample.setEndDate(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-09"));

        assertThat(employeeInProjectResult, samePropertyValuesAs(employeeInProjectSample));
    }

    @Test
    void testAddEmployeeToProjectSuccessfullyProjectHasNoEmployees() throws ParseException {
        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));
        projectSample.setEmployees(null);

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 8);
        EmployeeInProject employeeInProjectResult = projectResult.getEmployees().iterator().next();
        employeeInProjectSample.setEndDate(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-09"));

        assertThat(employeeInProjectResult, samePropertyValuesAs(employeeInProjectSample));
    }

    @Test
    void testAddEmployeeToProjectSuccessfullyProjectHasOtherEmployees() throws ParseException {
        Employee otherEmployeeSample = new Employee();
        otherEmployeeSample.setId(2);
        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-02-10");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-02-11");
        EmployeeInProject otherEmployeeIOnProject = new EmployeeInProject(otherEmployeeSample, projectSample, 8, startDate, endDate);
        projectSample.setEmployees(new HashSet<>(Collections.singletonList(otherEmployeeIOnProject)));
        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findById(1L)).thenReturn(Optional.ofNullable(employeeSample));

        Project projectResult = projectService.addEmployeeToProject(1L, 1L, roleSample, 8);
        projectResult.getEmployees().remove(otherEmployeeIOnProject);
        EmployeeInProject employeeInProjectResult = projectResult.getEmployees().iterator().next();
        employeeInProjectSample.setEndDate(new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-09"));

        assertThat(employeeInProjectResult, samePropertyValuesAs(employeeInProjectSample));
    }

    @Test
    void testProposeEmployeesForProjectNotSuccessfullyProjectIsNull() {
        when(projectRepository.findById(1L)).thenReturn(Optional.empty());

        List<Employee> result = projectService.proposeEmployeesForProject(1);

        Assertions.assertNull(result);
    }

    @Test
    void testProposeEmployeesForProjectNotSuccessfullyNoEmployeesAvailable() {
        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(projectRepository.findAll()).thenReturn(Collections.emptyList());
        List<Employee> result = projectService.proposeEmployeesForProject(1);

        Assertions.assertNull(result);
    }

    @Test
    void testProposeEmployeesForProjectNotSuccessfullyNoSuitedEmployeesFound() {
        Role roleSample1 = new Role();
        roleSample1.setName("role1");
        Role roleSample2 = new Role();
        roleSample1.setName("role2");

        Employee employeeSample1 = new Employee();
        employeeSample1.setId(2);
        employeeSample1.setRoles(new HashSet<>(Collections.singletonList(roleSample1)));
        Employee employeeSample2 = new Employee();
        employeeSample2.setRoles(new HashSet<>(Collections.singletonList(roleSample2)));
        employeeSample2.setId(3);

        List<Employee> employees = new ArrayList<>();
        employees.add(employeeSample1);
        employees.add(employeeSample2);

        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findAll()).thenReturn(employees);

        List<Employee> result = projectService.proposeEmployeesForProject(1);

        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void testProposeEmployeesForProjectNotSuccessfully() {
        Role roleSample1 = new Role();
        roleSample1.setName("role1");
        Role roleSample2 = new Role();
        roleSample1.setName("role2");

        Employee employeeSample1 = new Employee();
        employeeSample1.setId(2);
        employeeSample1.setRoles(new HashSet<>(Collections.singletonList(roleSample1)));
        Employee employeeSample2 = new Employee();
        employeeSample2.setRoles(new HashSet<>(Collections.singletonList(roleSample1)));
        employeeSample2.setId(3);

        List<Employee> employees = new ArrayList<>();
        employees.add(employeeSample1);
        employees.add(employeeSample2);
        employees.add(employeeSample);

        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));
        when(employeeRepository.findAll()).thenReturn(employees);

        List<Employee> result = projectService.proposeEmployeesForProject(1);

        Assertions.assertEquals(result.size(), 1);
    }

    @Test
    void getEmployeesInProjectNotSuccessfullyProjectIsNull() {
        when(projectRepository.findById(1L)).thenReturn(Optional.empty());

        Set<EmployeeInProject> result = projectService.getEmployeesInProject(1);

        Assertions.assertNull(result);
    }

    @Test
    void getEmployeesInProjectSuccessfully() {
        when(projectRepository.findById(1L)).thenReturn(Optional.ofNullable(projectSample));

        Set<EmployeeInProject> result = projectService.getEmployeesInProject(1);

        Assertions.assertNotNull(result);
    }
}